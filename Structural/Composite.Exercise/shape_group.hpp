#ifndef SHAPEGROUP_HPP
#define SHAPEGROUP_HPP

#include <memory>
#include <vector>

#include "shape.hpp"

namespace Drawing
{    
    using ShapePtr = std::unique_ptr<Shape>;

    class ShapeGroup : public CloneableShape<ShapeGroup>
    {
        std::vector<ShapePtr> shapes_;
    public:
        using iterator = std::vector<ShapePtr>::iterator;
        using const_iterator = std::vector<ShapePtr>::const_iterator;

        constexpr static auto id = "ShapeGroup";

        ShapeGroup() = default;

        ShapeGroup(const ShapeGroup& src)
        {
            for(const auto& s : src.shapes_)
                shapes_.push_back(s->clone());
        }

        ShapeGroup& operator=(const ShapeGroup& src)
        {
            ShapeGroup temp(src);
            swap(temp);

            return *this;
        }

        ShapeGroup(ShapeGroup&&) = default;
        ShapeGroup& operator=(ShapeGroup&&) = default;

        void swap(ShapeGroup& other)
        {
            shapes_.swap(other.shapes_);
        }

        void move(int dx, int dy) override
        {
            for(const auto& s : shapes_)
                s->move(dx, dy);
        }

        void draw() const override
        {
            for(const auto& s : shapes_)
                s->draw();
        }

        void add(ShapePtr shape)
        {
            shapes_.push_back(std::move(shape));
        }

        size_t size() const
        {
            return shapes_.size();
        }

        iterator begin()
        {
            return shapes_.begin();
        }

        const_iterator begin() const
        {
            return shapes_.begin();
        }

        iterator end()
        {
            return shapes_.end();
        }

        const_iterator end() const
        {
            return shapes_.end();
        }
    };
}

#endif // SHAPEGROUP_HPP
