#include "shape_group_reader_writer.hpp"
#include "../shape_factories.hpp"

using namespace std;
using namespace Drawing;
using namespace Drawing::IO;

namespace
{
    bool is_registered = SingletonShapeRWFactory::instance()
            .register_creator(make_type_index<ShapeGroup>(),
                []{ return make_unique<ShapeGroupReaderWriter>(
                    SingletonShapeFactory::instance(),
                    SingletonShapeRWFactory::instance()); });

}


ShapeGroupReaderWriter::ShapeGroupReaderWriter(ShapeFactory& shape_factory, ShapeRWFactory& shape_rw_factory)
    : shape_factory_{shape_factory}, shape_rw_factory_{shape_rw_factory}
{
}

void ShapeGroupReaderWriter::read(Shape& shp, istream& in)
{
    ShapeGroup& sg = static_cast<ShapeGroup&>(shp);

    int count;
    in >> count;

    for(int i = 0; i < count; ++i)
    {
        string shape_id;
        in >> shape_id;

        auto shp = shape_factory_.create(shape_id);
        auto shp_rw = shape_rw_factory_.create(make_type_index(*shp));

        shp_rw->read(*shp, in);
        sg.add(move(shp));
    }
}

void ShapeGroupReaderWriter::write(const Shape& shp, ostream& out)
{
    const ShapeGroup& sg = static_cast<const ShapeGroup&>(shp);

    out << ShapeGroup::id << " " << sg.size() << endl;

    for(const auto& s : sg)
    {
        auto shp_rw = shape_rw_factory_.create(make_type_index(*s));
        shp_rw->write(*s, out);
    }
}
