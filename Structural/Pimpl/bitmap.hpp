#ifndef BITMAP_HPP
#define BITMAP_HPP

#include <algorithm>
#include <iostream>
#include <vector>
#include <memory>

class Bitmap;

class Bitmap
{
    class BitmapImpl;

    std::unique_ptr<BitmapImpl> impl_;
public:    
    Bitmap(char fill_char = '*');

    Bitmap(const Bitmap&) = delete;
    Bitmap& operator=(const Bitmap&) = delete;

    Bitmap(Bitmap&&) = default;
    Bitmap& operator=(Bitmap&&) = default;

    ~Bitmap();

    void draw();
};



#endif // BITMAP_HPP
