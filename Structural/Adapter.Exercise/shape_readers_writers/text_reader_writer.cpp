#include "text_reader_writer.hpp"
#include "../shape_factories.hpp"
#include "../text.hpp"

using namespace std;
using namespace Drawing;
using namespace IO;

// TODO - rejestracja fabryki dla TextReaderWriter
namespace
{
    bool is_registered = SingletonShapeRWFactory::instance()
            .register_creator(make_type_index<Text>(), &make_unique<TextReaderWriter>);
}

void TextReaderWriter::read(Shape& shp, istream& in)
{
    Text& txt = static_cast<Text&>(shp);

    Point pt;
    string str;

    in >> pt >> str;

    txt.set_coord(pt);
    txt.set_text(str);
}

void TextReaderWriter::write(const Shape& shp, ostream& out)
{
    const Text& txt = static_cast<const Text&>(shp);

    out << Text::id << " " << txt.coord() << " " << txt.text() << endl;
}
