#include "text.hpp"
#include "shape_factories.hpp"

namespace
{
    using namespace std;
    using namespace Drawing;

    bool is_registered = SingletonShapeFactory::instance()
            .register_creator(Text::id, &make_unique<Text>);
}
