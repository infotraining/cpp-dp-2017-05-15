#ifndef TEXT_HPP
#define TEXT_HPP

#include "paragraph.hpp"
#include "shape.hpp"
#include <string>

namespace Drawing
{

    // TODO - zaadaptowac klase Paragraph do wymogow klienta
    class Text : public ShapeBase, private LegacyCode::Paragraph
    {
    public:
        constexpr static auto id = "Text";

        Text(int x = 0, int y = 0, const std::string& txt = "")
            : ShapeBase{x, y}, LegacyCode::Paragraph{txt.c_str()}
        {}

        void draw() const override
        {
            render_at(this->coord().x, this->coord().y);
        }

        void set_text(const std::string& txt)
        {
            set_paragraph(txt.c_str());
        }

        std::string text() const
        {
            return get_paragraph();
        }
    };
}

#endif
