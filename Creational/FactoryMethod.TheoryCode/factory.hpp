#ifndef FACTORY_HPP_
#define FACTORY_HPP_

#include <iostream>
#include <memory>
#include <string>

// "Product"
class Service
{
public:
    // product's interface
    virtual std::string run() const = 0;
    virtual ~Service() {}
};

// "ConcreteProductA"
class ConcreteServiceA : public Service
{
public:
    std::string run() const override
    {
        return std::string("ConcreteServiceA::run()");
    }
};

class ConcreteServiceC : public Service
{
    // Service interface
public:
    std::__cxx11::string run() const override
    {
        return std::string("ConcreteServiceC::run()");
    }
};

// "ConcreteProductB"
class ConcreteServiceB : public Service
{
public:
    std::string run() const override
    {
        return std::string("ConcreteServiceB::run()");
    }
};

namespace Canonical
{

// "Creator"
class ServiceCreator
{
public:
    virtual std::unique_ptr<Service> create_service() = 0; // factory method
    virtual ~ServiceCreator() = default;
};

// "ConcreteCreator"
class ConcreteCreatorA : public ServiceCreator
{
public:
    virtual std::unique_ptr<Service> create_service() override
    {
        return std::make_unique<ConcreteServiceA>();
    }
};

// "ConcreteCreator"
class ConcreteCreatorB : public ServiceCreator
{
public:
    std::unique_ptr<Service> create_service() override
    {
        return std::make_unique<ConcreteServiceB>();
    }
};

class ConcreteCreatorC : public ServiceCreator
{
    // ServiceCreator interface
public:
    std::unique_ptr<Service> create_service() override
    {
        return std::make_unique<ConcreteServiceC>();
    }
};

}

using ServiceCreator = std::function<std::unique_ptr<Service>()>;

#endif /*FACTORY_HPP_*/
