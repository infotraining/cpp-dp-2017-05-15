#include <cstdlib>
#include <iostream>
#include <map>
#include <string>
#include <vector>
#include <list>

#include "factory.hpp"

using namespace std;

namespace Canonical
{

class Client
{
    shared_ptr<ServiceCreator> creator_;

public:
    Client(shared_ptr<ServiceCreator> creator) : creator_(creator)
    {
    }

    Client(const Client&) = delete;
    Client& operator=(const Client&) = delete;

    void use()
    {
        unique_ptr<Service> service = creator_->create_service();

        string result = service->run();
        cout << "Client is using: " << result << endl;
    }
};

typedef std::map<std::string, shared_ptr<ServiceCreator> > Factory;

}

class Client
{
    ServiceCreator creator_;

public:
    Client(ServiceCreator creator) : creator_(creator)
    {
    }

    Client(const Client&) = delete;
    Client& operator=(const Client&) = delete;

    void use()
    {
        unique_ptr<Service> service = creator_();

        string result = service->run();
        cout << "Client is using: " << result << endl;
    }
};

typedef std::map<std::string, ServiceCreator> Factory;

int main()
{
    Factory creators;
    creators.insert(make_pair("ServiceA", [] { return std::make_unique<ConcreteServiceA>(); }));
    creators.insert(make_pair("ServiceB", &std::make_unique<ConcreteServiceB>));
    creators.insert(make_pair("ServiceC", &std::make_unique<ConcreteServiceC>));

    Client client(creators["ServiceC"]);
    client.use();

    using Data = list<int>;

    Data data{ 1, 2, 3 };

    for(const auto& item : data)
    {
        cout << item << "\n";
    }

    for(auto it = data.begin(); it != data.end(); ++it)
    {
        const auto& item = *it;
        cout << item << "\n";
    }
}
