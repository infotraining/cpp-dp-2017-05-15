#include "bank_account.hpp"

using namespace std;

const OverdraftState BankAccount::overdraft_state_;
const NormalState BankAccount::normal_state_;

int main()
{
	BankAccount ba1(1);
    cout << ba1.status() << endl;
	ba1.deposit(10000.0);
    cout << ba1.status() << endl;
	ba1.withdraw(5000.0);
    cout << ba1.status() << endl;
	ba1.pay_interest();
    cout << ba1.status() << endl;
	ba1.withdraw(10000.0);
    cout << ba1.status() << endl;
	ba1.pay_interest();
    cout << ba1.status() << endl;
	ba1.withdraw(1000.0);
    cout << ba1.status() << endl;
	ba1.deposit(7000.0);
    cout << ba1.status() << endl;
}
