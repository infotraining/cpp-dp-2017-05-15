#ifndef BANK_ACCOUNT_HPP
#define BANK_ACCOUNT_HPP

#include <iostream>
#include <string>
#include <cassert>
#include <memory>
#include <functional>
#include <sstream>

struct AccountData
{
    int id{};
    double balance{};

    AccountData(int id = -1, double balance = 0.0) : id{id}, balance{balance}
    {}
};

class AccountState
{
public:
    virtual ~AccountState() = default;
    virtual void withdraw(double amount, AccountData& account_context) const = 0;
    virtual void pay_interest(AccountData& account_context) const = 0;
    virtual std::string status() const = 0;
};

class NormalState : public AccountState
{
    constexpr static auto id = "Normal";
public:
    void withdraw(double amount, AccountData& account_context) const override
    {
        account_context.balance -= amount;
    }

    void pay_interest(AccountData& account_context) const override
    {
        account_context.balance += account_context.balance * 0.05;
    }

    std::string status() const override
    {
        return id;
    }
};

class OverdraftState : public AccountState
{
    // AccountState interface
    constexpr static auto id = "Overdraft";
public:
    void withdraw(double amount, AccountData& account_context) const override
    {
        std::cout << "Brak srodkow na koncie #" << account_context.id <<  std::endl;
    }

    void pay_interest(AccountData& account_context) const override
    {
        account_context.balance += account_context.balance * 0.15;
    }

    std::string status() const override
    {
        return id;
    }
};

class BankAccount
{      
    AccountData data_context_;
    const AccountState* state_;

    static const OverdraftState overdraft_state_;
    static const NormalState normal_state_;
protected:
	void update_account_state()
	{
        if (data_context_.balance < 0)
            state_ = &overdraft_state_;
		else
            state_ = &normal_state_;
	}

public:
    BankAccount(int id) : data_context_{id}, state_(&normal_state_) {}

	void withdraw(double amount)
	{
		assert(amount > 0);

        state_->withdraw(amount, data_context_);

        update_account_state();
	}

	void deposit(double amount)
	{
		assert(amount > 0);

        data_context_.balance += amount;

		update_account_state();
	}

	void pay_interest()
	{
        state_->pay_interest(data_context_);
	}

    std::string status() const
	{
        std::stringstream strm;
        strm << "BankAccount #" << data_context_.id << "; State: ";

        strm << state_->status() << "; ";

        strm << "Balance: " << data_context_.balance;

        return strm.str();
	}

	double balance() const
	{
        return data_context_.balance;
	}

	int id() const
	{
        return data_context_.id;
	}
};

#endif
