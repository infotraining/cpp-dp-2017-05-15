#include "template_method.hpp"

using namespace std;

void client(AbstractClass& ac)
{
    ac.template_method();
}

int main()
{
    unique_ptr<AbstractClass> ac = make_unique<ConcreteClassA>();
    client(*ac);

    std::cout << "\n\n";

    ac = make_unique<ConcreteClassB>();
    client(*ac);
};
