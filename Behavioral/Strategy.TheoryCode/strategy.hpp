#ifndef STRATEGY_HPP_
#define STRATEGY_HPP_

#include <algorithm>
#include <cstring>
#include <functional>
#include <iostream>
#include <memory>

// "Strategy"
class Strategy
{
public:
    virtual void algorithm_interface(const std::string& data) = 0;
    virtual ~Strategy() = default;
};

// "ConcreteStrategyA"
class ConcreteStrategyA : public Strategy
{
public:
    void algorithm_interface(const std::string& data) override
    {
        std::string transformed_data{data};

        std::transform(data.begin(), data.end(), transformed_data.begin(), [](char c) { return std::toupper(c); });

        std::cout << "Data: " << transformed_data << "\n";
    }
};

// "ConcreteStrategyB"
class ConcreteStrategyB : public Strategy
{
public:
    virtual void algorithm_interface(const std::string& data) override
    {
        std::string transformed_data{data};

        std::transform(data.begin(), data.end(), transformed_data.begin(), [](char c) { return std::tolower(c); });

        std::cout << "Data: " << transformed_data << "\n";
    }
};

// "ConcreteStrategyC"
class ConcreteStrategyC : public Strategy
{
public:
    virtual void algorithm_interface(const std::string& data) override
    {
        std::string transformed_data{data};

        if (data.size() >= 1)
        {
            transformed_data.front() = std::toupper(data.front());

            std::transform(data.begin() + 1, data.end(), transformed_data.begin() + 1, [](char c) { return std::tolower(c); });
        }

        std::cout << "Data: " << transformed_data << "\n";
    }
};

// "Context"
class Context
{
    std::shared_ptr<Strategy> strategy_;
    std::string data_ = "text";

public:
    Context(std::shared_ptr<Strategy> strategy) : strategy_{strategy}
    {
    }

    void reset_strategy(std::shared_ptr<Strategy> new_strategy)
    {
        strategy_ = new_strategy;
    }

    void context_interface()
    {
        strategy_->algorithm_interface(data_);
    }
    std::string data() const
    {
        return data_;
    }

    void set_data(const std::string& data)
    {
        data_ = data;
    }
};

#endif /*STRATEGY_HPP_*/
