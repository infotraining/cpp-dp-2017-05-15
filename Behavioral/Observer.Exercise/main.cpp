#include "stock.hpp"

using namespace std;

int main()
{
	Stock misys("Misys", 340.0);
	Stock ibm("IBM", 245.0);
	Stock tpsa("TPSA", 95.0);

    Investor kulczyk_jr{"Kulczyk Jr"};
    Investor solorz{"Solorz"};

    misys.connect([&kulczyk_jr](const string& symbol, double price) { kulczyk_jr.update_portfolio(symbol, price);});
    tpsa.connect([&kulczyk_jr](const string& symbol, double price) { kulczyk_jr.update_portfolio(symbol, price);});
    auto conn = misys.connect([&solorz](const string& symbol, double price) { solorz.update_portfolio(symbol, price);});

	// zmian kursow
	misys.set_price(360.0);
	ibm.set_price(210.0);
	tpsa.set_price(45.0);

    cout << "\n\n";
    conn.disconnect();

	misys.set_price(380.0);
	ibm.set_price(230.0);
	tpsa.set_price(15.0);
}
