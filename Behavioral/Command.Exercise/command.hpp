#ifndef COMMAND_HPP_
#define COMMAND_HPP_

#include <iostream>
#include <string>
#include <stack>
#include <memory>

#include "document.hpp"

class Command
{
public:
    virtual void execute() = 0;
    virtual ~Command() = default;
};

class UndoableCommand : public Command
{
public:
    virtual void undo() = 0;
    virtual std::unique_ptr<UndoableCommand> clone() const = 0;
};

using UndoableCommandPtr = std::unique_ptr<UndoableCommand>;

class CommandHistory
{   
    std::stack<UndoableCommandPtr> commands_;
public:
    void record_command(UndoableCommandPtr cmd)
    {
        commands_.push(move(cmd));
    }

    UndoableCommandPtr retrieve_last_command()
    {
        if (commands_.empty())
            throw std::out_of_range("No commands in history");

        UndoableCommandPtr last_cmd = move(commands_.top());
        commands_.pop();

        return last_cmd;
    }
};


// CRTP for clone()
template <typename Type>
class UndoableCommandBase : public UndoableCommand
{
    CommandHistory& cmd_tracker_;
public:
    UndoableCommandBase(CommandHistory& cmd_tracker) : cmd_tracker_(cmd_tracker)
    {}

    void execute() override final
    {
        do_save_state();

        cmd_tracker_.record_command(clone());

        do_execute();
    }

    void undo() override final
    {
        do_undo();
    }

    UndoableCommandPtr clone() const override
    {
        return std::make_unique<Type>(static_cast<const Type&>(*this));
    }

protected:
    virtual void do_save_state() = 0;
    virtual void do_execute() = 0;
    virtual void do_undo() = 0;
};

class PasteCmd : public UndoableCommandBase<PasteCmd>
{
    Document& document_;
    size_t prev_length_;

public:
    PasteCmd(Document& document, CommandHistory& cmd_tracker)
        : UndoableCommandBase(cmd_tracker), document_(document), prev_length_(0)
    {
    }

protected:
    void do_save_state() override
    {
        prev_length_ = document_.length();
    }

    void do_execute() override
    {
        document_.paste();
    }

    void do_undo() override
    {
        size_t replacement_length = document_.length() - prev_length_;
        document_.replace(prev_length_, replacement_length, "");
    }
};

class CopyCmd : public Command
{
    Document& document_;
public:
    CopyCmd(Document& doc) : document_{doc}
    {}

    void execute() override
    {
        document_.copy();
    }
};

class ToUpperCmd : public UndoableCommandBase<ToUpperCmd>
{
    Document& document_;
    Document::Memento memento_;

public:
    ToUpperCmd(Document& document, CommandHistory& cmd_tracker)
        : UndoableCommandBase(cmd_tracker), document_(document)
    {
    }

protected:
    void do_save_state() override
    {
        memento_ = document_.create_memento();
    }

    void do_execute() override
    {
        document_.to_upper();
    }

    void do_undo() override
    {
        document_.set_memento(std::move(memento_));
    }
};

class ToLowerCmd : public UndoableCommandBase<ToUpperCmd>
{
    Document& document_;
    Document::Memento memento_;

public:
    ToLowerCmd(Document& document, CommandHistory& cmd_tracker)
        : UndoableCommandBase(cmd_tracker), document_(document)
    {
    }

protected:
    void do_save_state() override
    {
        memento_ = document_.create_memento();
    }

    void do_execute() override
    {
        document_.to_lower();
    }

    void do_undo() override
    {
        document_.set_memento(std::move(memento_));
    }
};

class PrintCmd : public Command
{
    Document& document_;
    Printer& printer_;
public:
    PrintCmd(Document& document, Printer& printer) : document_(document), printer_(printer)
    {
    }

    void execute()
    {
        printer_.print("'" + document_.title() + "' - [" + document_.text() + "]");
    }
};


class AddTextCmd : public UndoableCommandBase<AddTextCmd>
{
    Document& document_;
    size_t prev_length_;
public:
    AddTextCmd(Document& document, CommandHistory& cmd_tracker)
        : UndoableCommandBase(cmd_tracker), document_(document)
    {
    }
protected:
    void do_save_state() override
    {
        prev_length_ = document_.length();
    }

    void do_execute() override
    {
        std::cout << "Enter text: ";
        std::cin.ignore();
        std::string txt;
        std::getline(std::cin, txt);

        document_.add_text(txt);
    }

    void do_undo() override
    {
        size_t replacement_length = document_.length() - prev_length_;
        document_.replace(prev_length_, replacement_length, "");
    }
};


class UndoCmd : public Command
{
    Document& document_;
    CommandHistory& cmd_tracker_;
public:
    UndoCmd(Document& doc, CommandHistory& cmd_tracker) : document_(doc), cmd_tracker_(cmd_tracker) {}

    void execute()
    {
        try
        {
            auto prevCmd = cmd_tracker_.retrieve_last_command();
            prevCmd->undo();
        }
        catch(const std::out_of_range& e)
        {
            std::cout << e.what() << std::endl;
        }
    }
};

#endif /*COMMAND_HPP_*/
