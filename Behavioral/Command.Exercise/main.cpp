#include "document.hpp"
#include "command.hpp"
#include "application.hpp"

using namespace std;

class DocApplication : public Application
{
    Document doc_;
    CommandHistory cmd_tracker_;
    Printer printer_;
public:
    DocApplication() : doc_{"Wzorzec projektowy: Command"}
    {
        // obiekty polecen
        unique_ptr<Command> cmd_paste = make_unique<PasteCmd>(doc_, cmd_tracker_);
        unique_ptr<Command> cmd_undo = make_unique<UndoCmd>(doc_, cmd_tracker_);
        unique_ptr<Command> cmd_print = make_unique<PrintCmd>(doc_, printer_);
        unique_ptr<Command> cmd_toupper = make_unique<ToUpperCmd>(doc_, cmd_tracker_);
        unique_ptr<Command> cmd_copy = make_unique<CopyCmd>(doc_);
        auto cmd_tolower = make_unique<ToLowerCmd>(doc_, cmd_tracker_);
        auto cmd_addtext = make_unique<AddTextCmd>(doc_, cmd_tracker_);

        // przyciski
        auto menu_paste = MenuItem {"Paste", move(cmd_paste)};
        auto menu_toupper = MenuItem {"ToUpper", move(cmd_toupper)};
        auto menu_undo = MenuItem {"Undo", move(cmd_undo)};
        auto menu_print = MenuItem {"Print", move(cmd_print)};
        auto menu_copy = MenuItem {"Copy", move(cmd_copy) };
        auto menu_tolower = MenuItem {"ToLower", move(cmd_tolower) };
        auto menu_add = MenuItem {"AddText", move(cmd_addtext) };

        add_menu(move(menu_paste));
        add_menu(move(menu_toupper));
        add_menu(move(menu_print));
        add_menu(move(menu_undo));
        add_menu(move(menu_copy));
        add_menu(move(menu_tolower));
        add_menu(move(menu_add));
    }
};

int main()
{
    DocApplication app;

    const std::string exit_application = "end";

    std::string cmd;
    do
    {
        std::cout << "Enter command: ";
        std::cin >> cmd;
        app.execute_action(cmd);
    } while (cmd != exit_application);

    std::cout << "End of application..." << std::endl;

    return 0;
}

